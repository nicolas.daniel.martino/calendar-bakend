FROM maven:3.8.3 AS base
COPY ./ /tmp/
WORKDIR /tmp/
RUN mvn clean compile

from base as test
RUN mvn test

from base as integration-test
run mvn verify $JAVA_OPTS