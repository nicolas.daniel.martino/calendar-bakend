package org.example.controllers.calendar.mapper;

import org.example.calendar.data.Calendar;
import org.example.controllers.calendar.dto.inbound.CalendarInboundDto;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CalendarDtoMapper {

    CalendarOutboundDto toDto(Calendar calendar);

    Calendar toDomain(CalendarInboundDto createCalendarInboundDto);
}
