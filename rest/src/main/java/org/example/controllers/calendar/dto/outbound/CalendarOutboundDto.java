package org.example.controllers.calendar.dto.outbound;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class CalendarOutboundDto {

    private Long id;

    private String title;

    private String description;
}
