package org.example.controllers.calendar;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import org.example.calendar.api.CalendarServiceApi;
import org.example.calendar.data.Calendar;
import org.example.config.exceptions.ResourceNotFoundException;
import org.example.config.security.ownership.CheckOwnership;
import org.example.config.security.ownership.ResourceName;
import org.example.controllers.calendar.dto.inbound.CalendarInboundDto;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.example.controllers.calendar.mapper.CalendarDtoMapper;
import org.example.user.api.UserServiceApi;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/calendars")
@Tag(name = "Calendars", description = "Operations on the calendars")
public class CalendarRestController {

    private final CalendarServiceApi calendarServiceApi;
    private final UserServiceApi userServiceApi;
    private final CalendarDtoMapper calendarDtoMapper;

    public CalendarRestController(CalendarServiceApi calendarServiceApi, UserServiceApi userServiceApi, CalendarDtoMapper calendarDtoMapper) {
        this.calendarServiceApi = calendarServiceApi;
        this.userServiceApi = userServiceApi;
        this.calendarDtoMapper = calendarDtoMapper;
    }

    @Operation(summary = "Create a calendar")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendar created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid calendar format is supplied",
                    content = @Content)})
    @PostMapping
    public CalendarOutboundDto addCalendar(@RequestBody @Valid CalendarInboundDto dto) {
        Calendar calendar = calendarDtoMapper.toDomain(dto);
        return calendarDtoMapper.toDto(calendarServiceApi.addCalendar(userServiceApi.getCurrent(), calendar));
    }

    @Operation(summary = "Finds a calendar")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendar found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id is supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Calendar not found",
                    content = @Content)
    })
    @GetMapping("/{calendarId}")
    @CheckOwnership(type = ResourceName.CALENDAR, id = "calendarId")
    public CalendarOutboundDto getCalendarByID(@PathVariable(name = "calendarId") Long calendarId) {
        return calendarServiceApi.getCalendarById(userServiceApi.getCurrent(), calendarId).map(calendarDtoMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException(calendarId));
    }

    @Operation(summary = "Updates a calendar")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendar updated successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid calendar format is supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Calendar not found",
                    content = @Content)
    })
    @PatchMapping("/{calendarId}")
    @CheckOwnership(type = ResourceName.CALENDAR, id = "calendarId")
    public CalendarOutboundDto updateCalendar(@PathVariable(name = "calendarId") Long calendarId, @RequestBody @Valid CalendarInboundDto dto) {
        Calendar calendarToUpdate = calendarDtoMapper.toDomain(dto);
        return calendarDtoMapper.toDto(calendarServiceApi.updateCalendar(userServiceApi.getCurrent(), calendarId, calendarToUpdate));
    }

    @Operation(summary = "Finds all calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendars found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "404", description = "No calendars not found",
                    content = @Content)
    })
    @GetMapping
    public List<CalendarOutboundDto> getAllCalendars() {
        return calendarServiceApi.getCalendars(userServiceApi.getCurrent()).stream().map(calendarDtoMapper::toDto).collect(Collectors.toList());
    }

    @Operation(summary = "Deletes a calendar")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendar deleted successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid calendar id is supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Calendar not found",
                    content = @Content)
    })
    @DeleteMapping("/{calendarId}")
    @CheckOwnership(type = ResourceName.CALENDAR, id = "calendarId")
    public void deleteCalendarByID(@PathVariable(name = "calendarId") Long calendarId) {
        calendarServiceApi.deleteCalendarById(userServiceApi.getCurrent(), calendarId);
    }

    @Operation(summary = "Searches all calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendars found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "404", description = "No calendars not found",
                    content = @Content)
    })
    @GetMapping("/search/fuzzy/{input}")
    public List<CalendarOutboundDto> multiFieldFuzzySearchForCurrentUser(@PathVariable(name = "input") String input) {
        return calendarServiceApi.multiFieldFuzzySearch(userServiceApi.getCurrent(), input).stream().map(calendarDtoMapper::toDto).collect(Collectors.toList());
    }

    @Operation(summary = "Searches top 5 suggested calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendars found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "404", description = "No calendars not found",
                    content = @Content)
    })
    @GetMapping("/search/suggest/{input}")
    public List<CalendarOutboundDto> top5SuggestionSearchForCurrentUser(@PathVariable(name = "input") @NotBlank @Size(min = 3) String input) {
        return calendarServiceApi.top5SuggestionSearch(userServiceApi.getCurrent(), input).stream().map(calendarDtoMapper::toDto).collect(Collectors.toList());
    }
}
