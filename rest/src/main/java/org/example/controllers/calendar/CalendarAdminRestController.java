package org.example.controllers.calendar;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.calendar.api.CalendarServiceApi;
import org.example.config.security.role.CheckRole;
import org.example.config.security.role.RoleEnum;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.example.controllers.calendar.mapper.CalendarDtoMapper;
import org.example.user.api.UserServiceApi;
import org.example.user.mapper.UserMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/admin/calendars")
@Tag(name = "Admin", description = "Admin operations")
public class CalendarAdminRestController {

    private final CalendarServiceApi calendarServiceApi;
    private final CalendarDtoMapper calendarDtoMapper;
    private final UserServiceApi userServiceApi;
    private final UserMapper userMapper;

    public CalendarAdminRestController(
            CalendarServiceApi calendarServiceApi,
            CalendarDtoMapper calendarDtoMapper,
            UserServiceApi userServiceApi,
            UserMapper userMapper) {
        this.calendarServiceApi = calendarServiceApi;
        this.calendarDtoMapper = calendarDtoMapper;
        this.userServiceApi = userServiceApi;
        this.userMapper = userMapper;
    }

    @Operation(summary = "Finds all calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendars found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "404", description = "No calendars not found",
                    content = @Content)
    })
    @GetMapping
    @CheckRole(RoleEnum.ADMIN)
    public List<CalendarOutboundDto> getAllCalendars() {
        return calendarServiceApi.getCalendars(userMapper.from(userServiceApi.getCurrent())).stream().map(calendarDtoMapper::toDto).collect(Collectors.toList());
    }

    @Operation(summary = "reindex all calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "reindexation starts",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
    })
    @GetMapping("/reindex-all")
    @CheckRole(RoleEnum.ADMIN)
    public void reindexAll() {
        calendarServiceApi.reindexAll();
    }

    @Operation(summary = "Searches all calendars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Calendars found successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CalendarOutboundDto.class))}),
            @ApiResponse(responseCode = "404", description = "No calendars not found",
                    content = @Content)
    })
    @GetMapping("/search/{input}")
    public List<CalendarOutboundDto> getAllCalendars(@PathVariable(name = "input") String input) {
        return calendarServiceApi.multiFieldFuzzySearch(userMapper.from(userServiceApi.getCurrent()), input).stream().map(calendarDtoMapper::toDto).collect(Collectors.toList());
    }
}
