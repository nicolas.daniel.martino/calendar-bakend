package org.example.controllers.calendar.dto.inbound;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalendarInboundDto {

    @NotEmpty
    @Size(min = 5, message = "title should have at least 2 characters")
    private String title;

    private String description;
}
