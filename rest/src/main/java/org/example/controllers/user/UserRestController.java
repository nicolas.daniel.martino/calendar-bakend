package org.example.controllers.user;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.config.security.permission.CheckPermission;
import org.example.config.security.permission.PermissionEnum;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/users")
@Tag(name = "Users", description = "Operations on the calendars users")
public class UserRestController {

    private final UserServiceApi userServiceApi;

    public UserRestController(UserServiceApi userServiceApi) {
        this.userServiceApi = userServiceApi;
    }

    @GetMapping("/current")
    @CheckPermission(PermissionEnum.VIEW_PROFILE)
    public User getCurrentUser() {
        return userServiceApi.getCurrent();
    }
}
