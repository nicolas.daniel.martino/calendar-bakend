package org.example.controllers;

import org.example.config.security.permission.CheckPermission;
import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.CheckRole;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.UserServiceApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestRestController {

    private final UserServiceApi userServiceApi;

    public TestRestController(UserServiceApi userServiceApi) {
        this.userServiceApi = userServiceApi;
    }

    @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    public ResponseEntity<String> getAnonymous() {
        return ResponseEntity.ok("Hello Anonymous");
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    @CheckRole(RoleEnum.ADMIN)
    public ResponseEntity<String> getAdmin() {
        return ResponseEntity.ok("Hello Admin");
    }

    @RequestMapping(value = "/promotor", method = RequestMethod.GET)
    @CheckPermission(PermissionEnum.VIEW_PROFILE)
    @CheckRole(RoleEnum.PROMOTOR)
    public ResponseEntity<String> getPromotor() {
        return ResponseEntity.ok("Hello promotor");
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @CheckPermission(PermissionEnum.VIEW_PROFILE)
    public ResponseEntity<String> getUser() {
        return ResponseEntity.ok("Hello promotor");
    }
}
