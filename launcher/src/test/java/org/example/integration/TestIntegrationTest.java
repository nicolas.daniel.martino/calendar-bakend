package org.example.integration;

import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {IntegrationTestConfiguration.class})
@ExtendWith(MockitoExtension.class)
public class TestIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    CalendarIntegrationTestAuthHelper authHelper;

    @BeforeEach
    public void setUp() {

    }

    @Test
    void should_respond_with_test_string_for_anonymous_user() {
        webTestClient.get().uri("/test/anonymous").exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(String.class)
                .isEqualTo("Hello Anonymous");
    }

    @Test
    void should_respond_with_test_string_for_admin_user() {
        getResource("/test/admin", authHelper.getAdminToken())
                .is2xxSuccessful()
                .expectBody(String.class)
                .isEqualTo("Hello Admin");
    }

    @Test
    void should_respond_with_test_string_for_promotor_user() {
        getResource("/test/promotor", authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBody(String.class)
                .isEqualTo("Hello promotor");
    }

    @Test
    void should_respond_with_user_info_for_promotor_user() {
        User user = getResource("api/v1/users/current", authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBody(User.class)
                .returnResult()
                .getResponseBody();

        Assertions.assertNotNull(user);
        Assertions.assertEquals(authHelper.getPromotorEmail(), user.getEmail());
        Assertions.assertTrue(UserServiceApi.checkPermissions(user.getPermissions(), PermissionEnum.VIEW_PROFILE, PermissionEnum.MANAGE_ACCOUNT));
        Assertions.assertTrue(UserServiceApi.checkRoles(user.getRoles(), RoleEnum.PROMOTOR));
    }

    @Test
    void should_respond_with_user_info_for_admin_user() {
        User user = getResource("api/v1/users/current", authHelper.getAdminToken())
                .is2xxSuccessful()
                .expectBody(User.class)
                .returnResult()
                .getResponseBody();

        Assertions.assertNotNull(user);
        Assertions.assertEquals(authHelper.getAdminEmail(), user.getEmail());
        Assertions.assertTrue(UserServiceApi.checkPermissions(user.getPermissions(), PermissionEnum.VIEW_PROFILE, PermissionEnum.MANAGE_ACCOUNT));
        Assertions.assertTrue(UserServiceApi.checkRoles(user.getRoles(), RoleEnum.ADMIN));
    }

    @Test
    void should_throw_when_user_has_no_admin_role() {
        getResource("/test/admin", authHelper.getPromotorToken())
                .isUnauthorized()
        ;
    }

    @Test
    void should_throw_when_user_is_anonymous() {
        webTestClient.get().uri("api/v1/users/current").exchange()
                .expectStatus()
                .isUnauthorized();
    }

}
