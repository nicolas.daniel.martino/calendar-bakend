package org.example.integration;

import org.example.controllers.calendar.dto.inbound.CalendarInboundDto;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {IntegrationTestConfiguration.class})
@ExtendWith(MockitoExtension.class)
@ResetPersistence
public class CalendarSearchIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    WebTestClient webTestClient;

    @Autowired
    CalendarIntegrationTestAuthHelper authHelper;

    @Test
    void should_respond_with_fuzzy_search_result_for_user() {
        //Given
        //Calendars to search
        List<CalendarInboundDto> jacksparrowCalendars = Arrays.asList(CalendarInboundDto.builder()
                        .title("jack sparrow test calendar belong 1")
                        .description("test calendar belong 1")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 2")
                        .description("test calendar belong 2")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 3")
                        .description("test calendar jack sparrow belong 3")
                        .build()
        );

        // calendar not to get
        CalendarInboundDto charlesIngallsCalendar = CalendarInboundDto.builder()
                .title("test calendar jack sparrow not belong 4")
                .description("test calendar jack sparrow not belong 4")
                .build();

        jacksparrowCalendars.forEach(cal -> createCalendar(cal, authHelper.getPromotorToken()));
        createCalendar(charlesIngallsCalendar, authHelper.getUserToken());

        // when
        // should return 3 calendars but calendar number 2 and charles ingalls calendar
        List<CalendarOutboundDto> results = getResource(format("%s/search/fuzzy/%s", CALENDARS_ENDPOINT, "jack"), authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBodyList(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        //then
        assertNotNull(results);
        assertEquals(2, results.size());

        int count = 0;
        for (CalendarOutboundDto result : results) {
            assertNotEquals(result.getTitle(), charlesIngallsCalendar.getTitle());
            assertNotEquals(result.getDescription(), charlesIngallsCalendar.getDescription());

            for (CalendarInboundDto actual : jacksparrowCalendars) {
                if (result.getTitle().equals(actual.getTitle()) && result.getDescription().equals(actual.getDescription())) {
                    count++;
                }
            }
        }

        assertEquals(2, count);
    }

    @Test
    void should_respond_with_top5_search_result_for_user() {
        //Given
        //Calendars to search
        List<CalendarInboundDto> jacksparrowCalendars = Arrays.asList(CalendarInboundDto.builder()
                        .title("jack sparrow test calendar belong 1")
                        .description("test calendar belong 1")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 2")
                        .description("test calendar belong 2")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 3")
                        .description("test calendar jack sparrow belong 3")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 4")
                        .description("test calendar jack sparrow belong 3")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 5")
                        .description("test calendar jack sparrow belong 3")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 6")
                        .description("test calendar jack sparrow belong 3")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 7")
                        .description("test calendar jack sparrow belong 7")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 8")
                        .description("test calendar jack sparrow belong 8")
                        .build()
        );

        // calendar not to get
        CalendarInboundDto charlesIngallsCalendar = CalendarInboundDto.builder()
                .title("test calendar jack sparrow not belong")
                .description("test calendar jack sparrow not belong")
                .build();

        jacksparrowCalendars.forEach(cal -> createCalendar(cal, authHelper.getPromotorToken()));
        createCalendar(charlesIngallsCalendar, authHelper.getUserToken());

        // when
        // should return 7 calendars but calendar number 2 and charles ingalls calendar
        // but pagination means only get top 5 of 7
        List<CalendarOutboundDto> results = getResource(format("%s/search/suggest/%s", CALENDARS_ENDPOINT, "jack"), authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBodyList(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        //then
        assertNotNull(results);
        assertEquals(5, results.size());

        int count = 0;
        for (CalendarOutboundDto result : results) {
            assertNotEquals(result.getTitle(), charlesIngallsCalendar.getTitle());
            assertNotEquals(result.getDescription(), charlesIngallsCalendar.getDescription());

            for (CalendarInboundDto actual : jacksparrowCalendars) {
                if (result.getTitle().equals(actual.getTitle()) && result.getDescription().equals(actual.getDescription())) {
                    count++;
                }
            }
        }

        assertEquals(5, count);
    }
}
