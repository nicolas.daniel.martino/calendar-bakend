package org.example.integration;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

@ActiveProfiles("test")
@Component
public class CalendarIntegrationTestAuthHelper {
    // TODO user property source instad of long ctor
    private final String adminUsername;
    private final String adminPassword;
    private final String adminMail;

    private final String promotorUsername;
    private final String promotorEmail;
    private final String promotorPassword;

    private final String userUsername;
    private final String userEmail;
    private final String userPassword;

    private final URI authorizationURI;
    private final String authClientId;
    private final String authClientGrantType;

    public CalendarIntegrationTestAuthHelper(@Value("${test.admin.username}") String adminUsername,
                                             @Value("${test.admin.password}") String adminPassword,
                                             @Value("${test.admin.mail}") String adminMail,
                                             @Value("${test.promotor.username}") String promotorUsername,
                                             @Value("${test.promotor.email}") String promotorEmail,
                                             @Value("${test.promotor.password}") String promotorPassword,
                                             @Value("${test.user.username}") String userUsername,
                                             @Value("${test.user.email}") String userEmail,
                                             @Value("${test.user.password}") String userPassword,
                                             @Value("${oauth2.client.calendar.token-uri}") String authorizationURI,
                                             @Value("${oauth2.client.registration.calendar.client-id}") String authClientId,
                                             @Value("${oauth2.client.registration.calendar.grant-type}") String authClientGrantType) throws URISyntaxException {
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
        this.adminMail = adminMail;
        this.promotorUsername = promotorUsername;
        this.promotorEmail = promotorEmail;
        this.promotorPassword = promotorPassword;
        this.userUsername = userUsername;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.authorizationURI = new URIBuilder(authorizationURI).build();
        this.authClientId = authClientId;
        this.authClientGrantType = authClientGrantType;
    }

    protected String getPromotorEmail() {
        return promotorEmail;
    }

    protected String getAdminEmail() {
        return adminMail;
    }

    protected String getUserEmail() {
        return userEmail;
    }

    protected String getAdminToken() {
        return getAuthToken(adminUsername, adminPassword);
    }

    protected String getPromotorToken() {
        return getAuthToken(promotorUsername, promotorPassword);
    }

    protected String getUserToken() {
        return getAuthToken(userUsername, userPassword);
    }

    private String getAuthToken(String adminUsername, String adminPassword) {
        WebClient webclient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(
                HttpClient.create().wiretap(true)
        )).build();
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.put("grant_type", Collections.singletonList(authClientGrantType));
        formData.put("client_id", Collections.singletonList(authClientId));
        formData.put("username", Collections.singletonList(adminUsername));
        formData.put("password", Collections.singletonList(adminPassword));

        String result = webclient.post()
                .uri(authorizationURI)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData(formData))
                .retrieve()
                .bodyToMono(String.class)
                .block();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(result)
                .get("access_token")
                .toString();
    }
}
