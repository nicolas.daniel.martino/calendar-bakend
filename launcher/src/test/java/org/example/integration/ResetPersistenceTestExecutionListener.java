package org.example.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static java.lang.String.format;

/**
 * https://stackoverflow.com/questions/34617152/how-to-re-create-database-before-each-test-in-spring
 */
public class ResetPersistenceTestExecutionListener extends AbstractTestExecutionListener {

    private static final List<String> IGNORED_TABLES = List.of(
            "flyway_schema_history"
    );

    private static final String SQL_DISABLE_REFERENTIAL_INTEGRITY = "ALTER TABLE %s DISABLE TRIGGER ALL";
    private static final String SQL_ENABLE_REFERENTIAL_INTEGRITY = "ALTER TABLE %s ENABLE TRIGGER ALL";

    private static final String SQL_FIND_TABLE_NAMES = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='%s'";
    private static final String SQL_TRUNCATE_TABLE = "TRUNCATE TABLE %s.%s";

    private static final String SQL_FIND_SEQUENCE_NAMES = "SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='%s'";
    private static final String SQL_RESTART_SEQUENCE = "ALTER SEQUENCE %s.%s RESTART WITH 1";

    @Autowired
    private DataSource dataSource;

    @Value("${schema.name}")
    private String schema;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    private Map<String, Class> indexClasses = new HashMap<>();

    private void getIndexClassesFromSpringContext() throws ClassNotFoundException {
        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);

        scanner.addIncludeFilter(new AnnotationTypeFilter(Document.class));

        for (BeanDefinition bd : scanner.findCandidateComponents("org.example")) {
            String className = ((ScannedGenericBeanDefinition) bd).getMetadata().getClassName();
            indexClasses.put(className, Class.forName(className));
        }
    }

    @Override
    public void beforeTestClass(TestContext testContext) {
        testContext.getApplicationContext()
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        try {
            getIndexClassesFromSpringContext();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beforeTestMethod(TestContext testContext) throws Exception {
        cleanupDatabase();
        cleanupSearchEngine();
    }

    private void cleanupSearchEngine() {
        for (Class indexClass : indexClasses.values()) {
            elasticsearchOperations.indexOps(indexClass).delete();
        }

        for (Class indexClass : indexClasses.values()) {
            elasticsearchOperations.indexOps(indexClass).create();
        }
    }

    private void cleanupDatabase() throws SQLException {
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement()
        ) {

            Set<String> tables = new HashSet<>();
            try (ResultSet resultSet = statement.executeQuery(format(SQL_FIND_TABLE_NAMES, schema))) {
                while (resultSet.next()) {
                    tables.add(resultSet.getString(1));
                }
            }

            for (String table : tables) {
                if (!IGNORED_TABLES.contains(table)) {
                    statement.executeUpdate(format(SQL_DISABLE_REFERENTIAL_INTEGRITY, table));
                    statement.executeUpdate(format(SQL_TRUNCATE_TABLE, schema, table));
                }
            }


            Set<String> sequences = new HashSet<>();
            try (ResultSet resultSet = statement.executeQuery(format(SQL_FIND_SEQUENCE_NAMES, schema))) {
                while (resultSet.next()) {
                    sequences.add(resultSet.getString(1));
                }
            }

            for (String sequence : sequences) {
                statement.executeUpdate(format(SQL_RESTART_SEQUENCE, schema, sequence));
            }

            for (String table : tables) {
                if (!IGNORED_TABLES.contains(table)) {
                    statement.executeUpdate(format(SQL_ENABLE_REFERENTIAL_INTEGRITY, table));
                }
            }
        }
    }
}
