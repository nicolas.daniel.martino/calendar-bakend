package org.example.integration;

import org.example.controllers.calendar.dto.inbound.CalendarInboundDto;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {IntegrationTestConfiguration.class})
@ExtendWith(MockitoExtension.class)
@ResetPersistence
public class CalendarIntegrationTest extends AbstractIntegrationTest {

    public static final CalendarInboundDto TEST_CALENDAR = CalendarInboundDto.builder()
            .title("test-calendar")
            .description("test calendar description")
            .build();

    @Autowired
    CalendarIntegrationTestAuthHelper authHelper;

    @Test
    void should_create_calendar_and_fetch_it() {
        //given
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getPromotorToken());


        // when
        CalendarOutboundDto getCalendarByIdResponse = getResource(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()), authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBody(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        assertNotNull(getCalendarByIdResponse);

        // Then
        assertEquals(TEST_CALENDAR.getTitle(), getCalendarByIdResponse.getTitle());
        assertEquals(TEST_CALENDAR.getDescription(), getCalendarByIdResponse.getDescription());
    }

    @Test
    void should_update_calendar() {
        //given
        CalendarInboundDto updateCalendarRequest = CalendarInboundDto.builder()
                .title("updated-test-calendar")
                .description("updated test calendar description")
                .build();

        // when
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getUserToken());

        CalendarOutboundDto updatedCalendar = webTestClient.patch().uri(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()))
                .headers(headers -> headers.setBearerAuth(authHelper.getUserToken()))
                .bodyValue(updateCalendarRequest)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        // Then
        assertNotNull(updatedCalendar);
        assertEquals(updateCalendarRequest.getTitle(), updatedCalendar.getTitle());
        assertEquals(updateCalendarRequest.getDescription(), updatedCalendar.getDescription());
    }

    @Test
    void should_delete_calendar() {
        //given
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getUserToken());

        // when
        webTestClient.delete().uri(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()))
                .headers(headers -> headers.setBearerAuth(authHelper.getUserToken()))
                .exchange()
                .expectStatus()
                .is2xxSuccessful();

        // TODO should be Not found?
        // Then
        getResource(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()), authHelper.getUserToken())
                .is5xxServerError();
    }


    @Test
    void should_get_all_calendars_belong_to_user() {
        //Given
        //Calendars to get
        List<CalendarInboundDto> jacksparrowCalendars = Arrays.asList(CalendarInboundDto.builder()
                        .title("test calendar belong 1")
                        .description("test calendar belong 1")
                        .build(),
                CalendarInboundDto.builder()
                        .title("test calendar belong 2")
                        .description("test calendar belong 2")
                        .build()
        );

        // calendar not to get
        CalendarInboundDto charlesIngallsCalendar = CalendarInboundDto.builder()
                .title("test calendar not belong 1")
                .description("test calendar not belong 1")
                .build();

        jacksparrowCalendars.forEach(cal -> createCalendar(cal, authHelper.getPromotorToken()));
        createCalendar(charlesIngallsCalendar, authHelper.getUserToken());

        // when
        List<CalendarOutboundDto> results = getResource(CALENDARS_ENDPOINT, authHelper.getPromotorToken())
                .is2xxSuccessful()
                .expectBodyList(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        //then
        assertNotNull(results);
        assertEquals(jacksparrowCalendars.size(), results.size());

        int count = 0;
        for (CalendarOutboundDto result : results) {
            for (CalendarInboundDto actual : jacksparrowCalendars) {
                if (result.getTitle().equals(actual.getTitle()) && result.getDescription().equals(actual.getDescription())) {
                    count++;
                }
            }
        }
        assertEquals(jacksparrowCalendars.size(), count);
    }

    @Test
    void should_not_fetch_calendar_with_unauthorised_user() {
        // given
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getPromotorToken());

        // TODO should be unauthorised or not found instead?
        // when & then
        getResource(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()), authHelper.getUserToken())
                .is5xxServerError();
    }

    @Test
    void should_not_update_calendar_with_unauthorised_user() {
        //given
        CalendarInboundDto updateCalendarRequest = CalendarInboundDto.builder()
                .title("updated-test-calendar")
                .description("updated test calendar description")
                .build();
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getUserToken());

        // when
        webTestClient.patch().uri(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()))
                .headers(headers -> headers.setBearerAuth(authHelper.getPromotorToken()))
                .bodyValue(updateCalendarRequest)
                .exchange()
                .expectStatus()
                .is5xxServerError();

        // TODO should be not found
        CalendarOutboundDto nonUpdatedCalendar = getResource(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()), authHelper.getUserToken())
                .is2xxSuccessful()
                .expectBody(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        // Then
        assertNotNull(nonUpdatedCalendar);
        assertEquals(TEST_CALENDAR.getTitle(), nonUpdatedCalendar.getTitle());
        assertEquals(TEST_CALENDAR.getDescription(), nonUpdatedCalendar.getDescription());
    }

    @Test
    void should_not_delete_calendar_with_unauthorised_user() {
        //given
        CalendarOutboundDto addCalendarResponse = createCalendar(TEST_CALENDAR, authHelper.getUserToken());

        // when
        webTestClient.delete().uri(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()))
                .headers(headers -> headers.setBearerAuth(authHelper.getPromotorToken()))
                .exchange()
                .expectStatus()
                .is5xxServerError();

        CalendarOutboundDto nonDeletedCalendar = getResource(format("%s/%d", CALENDARS_ENDPOINT, addCalendarResponse.getId()), authHelper.getUserToken())
                .is2xxSuccessful()
                .expectBody(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        // Then
        assertNotNull(nonDeletedCalendar);
        assertEquals(TEST_CALENDAR.getTitle(), nonDeletedCalendar.getTitle());
        assertEquals(TEST_CALENDAR.getDescription(), nonDeletedCalendar.getDescription());
    }
}
