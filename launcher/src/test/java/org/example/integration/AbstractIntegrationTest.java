package org.example.integration;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.example.controllers.calendar.dto.inbound.CalendarInboundDto;
import org.example.controllers.calendar.dto.outbound.CalendarOutboundDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.StatusAssertions;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


abstract class AbstractIntegrationTest {

    @Autowired
    WebTestClient webTestClient;

    @PersistenceContext
    EntityManager em;

    static final String CALENDARS_ENDPOINT = "api/v1/calendars";

    StatusAssertions getResource(String path, String token) {
        return webTestClient.get().uri(path)
                .headers(headers -> headers.setBearerAuth(token))
                .exchange()
                .expectStatus()
                ;
    }

    StatusAssertions postOneCalendar(CalendarInboundDto input, String token) {
        return webTestClient.post().uri(CALENDARS_ENDPOINT)
                .headers(headers -> headers.setBearerAuth(token))
                .bodyValue(input)
                .exchange()
                .expectStatus();
    }

    CalendarOutboundDto createCalendar(CalendarInboundDto calendarToCreate, String token) {
        CalendarOutboundDto addCalendarResponse = postOneCalendar(calendarToCreate, token)
                .is2xxSuccessful()
                .expectBody(CalendarOutboundDto.class)
                .returnResult()
                .getResponseBody();

        assertNotNull(addCalendarResponse);
        assertEquals(calendarToCreate.getTitle(), addCalendarResponse.getTitle());
        assertEquals(calendarToCreate.getDescription(), addCalendarResponse.getDescription());

        return addCalendarResponse;
    }
}
