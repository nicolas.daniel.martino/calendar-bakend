package org.example.config;

import org.example.user.data.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.jwt.Jwt;

public class KeycloakAuthenticationToken extends AbstractAuthenticationToken {

    private final User currentUser;

    private static final Logger LOG = LoggerFactory.getLogger(KeycloakAuthenticationToken.class);

    public KeycloakAuthenticationToken(Jwt jwt, User user) {
        super(user.getGrantedAuthorities());
        this.currentUser = user;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public User getPrincipal() {
        return currentUser;
    }

    @Override
    public boolean isAuthenticated() {
        if (!currentUser.getVerifiedEmail()) {
            LOG.warn("User with uuid {} has no verified email, user was unauthed", currentUser.getId());
            return false;
        }
        if (currentUser.getRoles().isEmpty()) {
            LOG.warn("User with uuid {} has no roles", currentUser.getId());
        }
        return true;
    }
}
