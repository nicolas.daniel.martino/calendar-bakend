package org.example.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static java.lang.String.format;

@Configuration
@Profile("!test")
public class OpenApiConfig {

    public static final String OPEN_API_CONF_ENDPONT = "/.well-known/openid-configuration";
    private final String maintainerEmail;
    private final String maintainerName;
    private final String openIdUrl;

    private static final String OPEN_ID_SCHEME_NAME = "openId";

    public OpenApiConfig(@Value("${maintainer.email") String maintainerEmail,
                         @Value("${maintainer.name}") String maintainerName,
                         @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}") String openIdUrl) {
        this.maintainerEmail = maintainerEmail;
        this.maintainerName = maintainerName;
        this.openIdUrl = openIdUrl;
    }

    @Bean
    public OpenAPI OpenApiConfig() {
        return new OpenAPI()
                .info(new Info()
                        .title("Calendar Application Programming Interface")
                        .description("This is a calendar api based on spec. https://www.w3.org/TR/2011/WD-calendar-api-20110419/")
                        .termsOfService("terms")
                        .contact(new Contact().email(maintainerEmail).name(format("Developer: %s", maintainerName)))
                        .license(new License().name("GNU"))
                        .version("2.0")
                )
                .addSecurityItem(new SecurityRequirement().addList(OPEN_ID_SCHEME_NAME))
                .components(
                        new Components()
                                .addSecuritySchemes(OPEN_ID_SCHEME_NAME, new SecurityScheme()
                                        .flows(new OAuthFlows().password(
                                                new OAuthFlow()
                                                        .authorizationUrl("http://localhost:8080/auth/realms/local/protocol/openid-connect/auth")
                                                        .tokenUrl("http://localhost:8080/auth/realms/local/protocol/openid-connect/token")
                                        ))
                                        .type(SecurityScheme.Type.OAUTH2)
                                        .description("Oauth2 flow")
                                )
                );
    }

}
