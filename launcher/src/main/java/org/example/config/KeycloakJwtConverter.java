package org.example.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.data.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class KeycloakJwtConverter implements Converter<Jwt, KeycloakAuthenticationToken> {

    private static final Logger LOG = LoggerFactory.getLogger(KeycloakJwtConverter.class);

    @Override
    public KeycloakAuthenticationToken convert(Jwt source) {
        User user = buildUserFromToken(source);
        return new KeycloakAuthenticationToken(source, user);
    }

    @Override
    public <U> Converter<Jwt, U> andThen(Converter<? super KeycloakAuthenticationToken, ? extends U> after) {
        return Converter.super.andThen(after);
    }

    User buildUserFromToken(Jwt token) {
        return User.builder()
                .id(UUID.fromString((String) token.getClaims().get("sub")))
                .email((String) token.getClaims().get("email"))
                .verifiedEmail((Boolean) token.getClaims().get("email_verified"))
                .permissions(parsePermissions(token))
                .roles(parseRoles(token))
                .build();
    }

    Set<PermissionEnum> parsePermissions(Jwt source) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode resourceAccess = objectMapper.convertValue(source.getClaimAsMap("resource_access"), JsonNode.class);

        LOG.debug("Begin parse permissions from resource access: jwt = {}", resourceAccess);
        Set<PermissionEnum> permissions = new HashSet<>();

        resourceAccess.path("account").path("roles").elements()
                .forEachRemaining(r -> {
                    try {
                        permissions.add(PermissionEnum.fromKebabCase(r.asText()));
                    } catch (IllegalArgumentException e) {
                        LOG.debug("Permission {} not found in PermissionEnum", r.asText());
                    }
                });

        LOG.debug("End parse permissions from resource access: jwt = {}", permissions);
        return permissions;
    }

    Set<RoleEnum> parseRoles(Jwt source) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode resourceAccess = objectMapper.convertValue(source.getClaimAsMap("realm_access"), JsonNode.class);

        LOG.debug("Begin extractRoles from realm access: jwt = {}", resourceAccess);
        Set<RoleEnum> roles = new HashSet<>();

        resourceAccess.path("roles").elements()
                .forEachRemaining(r -> {
                    try {
                        roles.add(RoleEnum.fromKebabCase(r.asText()));
                    } catch (IllegalArgumentException e) {
                        LOG.debug("Role {} not found in RolesEnum", r.asText());
                    }
                });

        LOG.debug("End parse permissions from realm access: jwt = {}", roles);
        return roles;
    }
}
