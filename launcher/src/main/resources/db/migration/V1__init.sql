CREATE SEQUENCE calendar_seq START WITH 0 CACHE 10 minvalue 0;

create table  if not exists calendar(
	id BIGINT not null,
	title varchar (255) not null,
	description varchar (255),
	CONSTRAINT PK_CALENDAR PRIMARY KEY (id)
);