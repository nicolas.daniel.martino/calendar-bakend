package org.example.calendar.search.documents;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import jakarta.persistence.Id;

import java.util.UUID;

@Data
@Builder
@Document(indexName = "calendar-#{@environment.getProperty('index.prefix')}")
public class CalendarDocument {
    @Id
    private Long id;

    @Field(type = FieldType.Text, name = CalendarFields.Values.USERID_VALUE)
    private UUID userId;

    @Field(type = FieldType.Text, name = CalendarFields.Values.TITLE_VALUE)
    private String title;

    @Field(type = FieldType.Text, name = CalendarFields.Values.DESCRIPTION_VALUE)
    private String description;

    public CalendarDocument(Long id, UUID userId, String title, String description) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.description = description;
    }

    public enum CalendarFields {
        ID(Values.ID_VALUE),
        USER_ID(Values.USERID_VALUE),
        TITLE(Values.TITLE_VALUE),
        DESCRIPTION(Values.DESCRIPTION_VALUE);

        CalendarFields(String name) {
        }

        public static class Values {
            public static final String ID_VALUE = "id";
            public static final String USERID_VALUE = "userId";
            public static final String TITLE_VALUE = "title";
            public static final String DESCRIPTION_VALUE = "description";
        }
    }
}
