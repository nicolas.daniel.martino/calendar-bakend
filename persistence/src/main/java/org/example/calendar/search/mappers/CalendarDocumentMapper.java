package org.example.calendar.search.mappers;

import org.example.calendar.data.Calendar;
import org.example.calendar.search.documents.CalendarDocument;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CalendarDocumentMapper {

    CalendarDocument toDocument(Calendar calendar);

    Calendar toDomain(CalendarDocument calendarDocument);
}
