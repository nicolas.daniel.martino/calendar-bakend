package org.example.calendar.search.repositories;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import org.example.calendar.search.documents.CalendarDocument;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.client.elc.NativeQueryBuilder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CalendarDocumentRepository extends ElasticsearchRepository<CalendarDocument, Long> {
    default NativeQuery multiFieldFuzzySearchQuery(String input, Optional<UUID> userId) {
        NativeQueryBuilder builder = NativeQuery.builder()
                .withQuery(query -> query
                        .multiMatch(mm ->
                                mm.fields(List.of(
                                        CalendarDocument.CalendarFields.Values.DESCRIPTION_VALUE,
                                        CalendarDocument.CalendarFields.Values.TITLE_VALUE)
                                ).query(input)
                        )
                );

        userId.ifPresent(uuid -> filterUserById(builder, uuid));

        return builder.build();
    }

    default NativeQuery top5SuggestionSearchQuery(String input, Optional<UUID> userId) {
        List<Query> queryList = new ArrayList<>();
        queryList.add(
                QueryBuilders.wildcard(w ->
                        w.field(CalendarDocument.CalendarFields.Values.TITLE_VALUE)
                                .value("*" + input + "*")
                )
        );
        queryList.add(
                QueryBuilders.wildcard(w ->
                        w.field(CalendarDocument.CalendarFields.Values.DESCRIPTION_VALUE)
                                .value("*" + input + "*")
                )
        );

        NativeQueryBuilder builder = NativeQuery.builder()
                .withQuery(q -> q.bool(b -> b.should(queryList)))
                .withPageable(PageRequest.of(0, 5));

        userId.ifPresent(uuid -> filterUserById(builder, uuid));

        return builder.build();
    }

    default void filterUserById(NativeQueryBuilder builder, UUID userId) {
        builder.withFilter(f ->
                f.bool(b ->
                        b.must(q ->
                                q.match(m ->
                                        m.field(CalendarDocument.CalendarFields.Values.USERID_VALUE).query(userId.toString()))
                        )
                )
        );
    }
}
