package org.example.calendar.jpa.mappers;

import org.example.calendar.data.Calendar;
import org.example.calendar.jpa.entities.CalendarEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CalendarMapperJpa {

    Calendar toDomain(CalendarEntity entity);

    CalendarEntity toEntity(Calendar domain);

}
