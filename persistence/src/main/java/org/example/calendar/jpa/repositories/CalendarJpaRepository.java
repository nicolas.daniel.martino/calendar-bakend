package org.example.calendar.jpa.repositories;


import org.example.calendar.jpa.entities.CalendarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CalendarJpaRepository extends JpaRepository<CalendarEntity, Long> {

    boolean existsByUserIdAndId(UUID userId, Long id);

    List<CalendarEntity> findAllByUserId(UUID userId);

    Optional<CalendarEntity> findByIdAndUserId(Long calendarId, UUID userId);
}
