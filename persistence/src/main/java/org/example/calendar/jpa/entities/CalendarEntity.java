package org.example.calendar.jpa.entities;

import lombok.*;

import jakarta.persistence.*;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "calendar")
public class CalendarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calendar_seq")
    @SequenceGenerator(name = "calendar_seq", sequenceName = "calendar_seq", allocationSize = 1)
    private Long id;

    @Column(name = "user_id")
    private UUID userId;

    private String title;

    private String description;

}
