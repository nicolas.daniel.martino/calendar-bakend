package org.example.calendar.adapters;

import jakarta.transaction.Transactional;
import org.example.calendar.data.Calendar;
import org.example.calendar.jpa.entities.CalendarEntity;
import org.example.calendar.jpa.mappers.CalendarMapperJpa;
import org.example.calendar.jpa.repositories.CalendarJpaRepository;
import org.example.calendar.search.documents.CalendarDocument;
import org.example.calendar.search.mappers.CalendarDocumentMapper;
import org.example.calendar.search.repositories.CalendarDocumentRepository;
import org.example.calendar.spi.CalendarPersistencePort;
import org.example.config.exceptions.ResourceNotFoundException;
import org.example.config.security.ownership.ResourceName;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.micrometer.common.util.StringUtils.isBlank;

@Service
@Transactional
public class CalendarPersistenceAdapter implements CalendarPersistencePort {

    private final CalendarJpaRepository calendarRepository;
    private final CalendarMapperJpa calendarMapperJpa;
    private final CalendarDocumentRepository calendarDocumentRepository;
    private final CalendarDocumentMapper calendarDocumentMapper;
    private final ElasticsearchOperations elasticsearchOperations;

    public CalendarPersistenceAdapter(CalendarJpaRepository calendarRepository,
                                      CalendarMapperJpa calendarMapperJpa,
                                      CalendarDocumentRepository calendarDocumentRepository,
                                      CalendarDocumentMapper calendarDocumentMapper, ElasticsearchOperations elasticsearchOperations) {
        this.calendarRepository = calendarRepository;
        this.calendarMapperJpa = calendarMapperJpa;
        this.calendarDocumentRepository = calendarDocumentRepository;
        this.calendarDocumentMapper = calendarDocumentMapper;
        this.elasticsearchOperations = elasticsearchOperations;
    }

    @Override
    public Calendar saveCalendar(Calendar calendar) {
        CalendarEntity calendarEntity = calendarMapperJpa.toEntity(calendar);

        Calendar persistedCalendar = calendarMapperJpa.toDomain(this.calendarRepository.save(calendarEntity));

        indexOne(persistedCalendar);
        return persistedCalendar;
    }


    @Override
    public void deleteCalendarById(Long id) {
        this.calendarRepository.deleteById(id);
        deleteIndexedOne(id);
    }

    @Override
    public Calendar updateCalendar(Calendar calendar) {
        Optional<CalendarEntity> entity = this.calendarRepository.findById(calendar.getId());

        if (entity.isEmpty()) {
            throw new ResourceNotFoundException("Calendar not found");
        }

        CalendarEntity calendarEntity = calendarMapperJpa.toEntity(calendar);

        reindexOne(calendar);
        return calendarMapperJpa.toDomain(this.calendarRepository.save(calendarEntity));
    }

    @Override
    public List<Calendar> findAllCalendars() {
        return this.calendarRepository.findAll().stream().map(calendarMapperJpa::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Calendar> findAllCalendarsByUserId(UUID userId) {
        return this.calendarRepository.findAllByUserId(userId).stream().map(calendarMapperJpa::toDomain).collect(Collectors.toList());
    }

    @Override
    public Optional<Calendar> findCalendarById(Long calendarId) {
        return this.calendarRepository.findById(calendarId).map(calendarMapperJpa::toDomain);
    }

    @Override
    public Optional<Calendar> findCalendarByIdAndUserId(Long calendarId, UUID userId) {
        return this.calendarRepository.findByIdAndUserId(calendarId, userId).map(calendarMapperJpa::toDomain);
    }

    @Override
    public boolean isOwner(UUID userId, Long calendarId) {
        return this.calendarRepository.existsByUserIdAndId(userId, calendarId);
    }

    @Override
    public ResourceName getResourceName() {
        return ResourceName.CALENDAR;
    }

    @Override
    public void reindexAll() {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            public void afterCommit() {
                calendarDocumentRepository.deleteAll();
                calendarDocumentRepository.saveAll(
                        calendarRepository.findAll().stream()
                                .map(calendarMapperJpa::toDomain)
                                .map(calendarDocumentMapper::toDocument)
                                .collect(Collectors.toList())
                );
            }
        });
    }

    @Override
    public void reindexOne(Calendar calendar) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            public void afterCommit() {
                calendarDocumentRepository.deleteById(calendar.getId());
                calendarDocumentRepository.save(calendarDocumentMapper.toDocument(calendar));
            }
        });
    }

    private void deleteIndexedOne(Long id) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            public void afterCommit() {
                calendarDocumentRepository.deleteById(id);
            }
        });
    }

    @Override
    public void indexOne(Calendar calendar) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            public void afterCommit() {
                calendarDocumentRepository.save(calendarDocumentMapper.toDocument(calendar));
            }
        });
    }

    @Override
    public List<Calendar> multiFieldFuzzySearch(String input, Optional<UUID> userId) {
        if (isBlank(input)) {
            throw new IllegalArgumentException("input search cannot be empty");
        }

        List<SearchHit<CalendarDocument>> result = elasticsearchOperations.search(calendarDocumentRepository.multiFieldFuzzySearchQuery(input, userId), CalendarDocument.class).getSearchHits();
        return result.stream().map(SearchHit::getContent).map(calendarDocumentMapper::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Calendar> top5SuggestionSearch(String input, Optional<UUID> userId) {
        if (isBlank(input)) {
            throw new IllegalArgumentException("input search cannot be empty");
        }

        SearchHits<CalendarDocument> result = elasticsearchOperations.search(calendarDocumentRepository.top5SuggestionSearchQuery(input, userId), CalendarDocument.class);
        return result.get().map(SearchHit::getContent).map(calendarDocumentMapper::toDomain).collect(Collectors.toList());
    }
}
