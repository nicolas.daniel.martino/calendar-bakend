package org.example.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import static java.lang.String.format;

@Configuration
@EnableElasticsearchRepositories(basePackages = {"org.example"} )
@ComponentScan(basePackages = {"org.example"})
public class ElasticSearchClientConfig extends ElasticsearchConfiguration {

    private final String elasticSearchHost;
    private final String elasticSearchPort;

    public ElasticSearchClientConfig(@Value("${search.host}") String elasticSearchHost,
                                     @Value("${search.port}") String elasticSearchPort) {
        this.elasticSearchHost = elasticSearchHost;
        this.elasticSearchPort = elasticSearchPort;
    }

    @Override
    public ClientConfiguration clientConfiguration() {
        return ClientConfiguration.builder()
                .connectedTo(format("%s:%s", elasticSearchHost, elasticSearchPort))
                .build();
    }
}
