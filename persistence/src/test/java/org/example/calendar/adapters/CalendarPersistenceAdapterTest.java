package org.example.calendar.adapters;

import org.example.calendar.data.Calendar;
import org.example.calendar.jpa.entities.CalendarEntity;
import org.example.calendar.jpa.mappers.CalendarMapperJpa;
import org.example.calendar.jpa.mappers.CalendarMapperJpaImpl;
import org.example.calendar.jpa.repositories.CalendarJpaRepository;
import org.example.calendar.search.documents.CalendarDocument;
import org.example.calendar.search.mappers.CalendarDocumentMapper;
import org.example.calendar.search.mappers.CalendarDocumentMapperImpl;
import org.example.calendar.search.repositories.CalendarDocumentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CalendarPersistenceAdapterTest {

    @Mock
    private CalendarJpaRepository calendarRepository;

    private final CalendarMapperJpa calendarMapperJpa = Mockito.mock(CalendarMapperJpaImpl.class);

    private CalendarDocumentMapper calendarDocumentMapper = Mockito.mock(CalendarDocumentMapperImpl.class);

    @Mock
    private CalendarDocumentRepository calendarDocumentRepository;

    @Mock
    private ElasticsearchOperations elasticsearchOperations;

    @InjectMocks
    private CalendarPersistenceAdapter calendarPersistenceAdapter;

    @BeforeEach
    void setUp() {
        TransactionSynchronizationManager.initSynchronization();
    }

    @AfterEach
    void tearDown() {
        TransactionSynchronizationManager.clear();
    }

    @Test
    void shouldSaveAndIndexCalendar() {
        // given
        Calendar calendar = Calendar.builder().title("test").build();
        CalendarEntity calendarEntity = CalendarEntity.builder().title("test").build();
        CalendarDocument calendarDocument = CalendarDocument.builder().title("test").build();


        when(calendarMapperJpa.toEntity(eq(calendar))).thenCallRealMethod();
        when(calendarMapperJpa.toDomain(eq(calendarEntity))).thenCallRealMethod();
        when(calendarDocumentMapper.toDocument(eq(calendar))).thenCallRealMethod();
        when(calendarRepository.save(calendarEntity)).thenReturn(calendarEntity);

        // when
        Calendar savedCalendar = calendarPersistenceAdapter.saveCalendar(calendar);
        TransactionSynchronizationManager.getSynchronizations().stream().findFirst().get().afterCommit();

        // then
        verify(calendarRepository, times(1)).save(eq(calendarEntity));
        assertEquals(calendar.getTitle(), savedCalendar.getTitle());

        verify(calendarDocumentRepository, times(1)).save(calendarDocument);
    }

    @Test
    void shouldDeleteCalendarAndIndexedObjectsById() {
        // given
        Long id = 1L;

        // when
        calendarPersistenceAdapter.deleteCalendarById(id);
        TransactionSynchronizationManager.getSynchronizations().stream().findFirst().get().afterCommit();

        // then
        verify(calendarRepository).deleteById(id);
        verify(calendarDocumentRepository, times(1)).deleteById(id);
    }

    @Test
    void shouldUpdateAndReindexCalendar() {
        // given
        Long id = 1L;
        Calendar calendar = Calendar.builder().id(id).title("Updated Test").build();
        CalendarEntity calendarEntity = CalendarEntity.builder().id(id).title("Updated Test").build();
        CalendarDocument calendarDocument = CalendarDocument.builder().id(id).title("Updated Test").build();

        when(calendarRepository.findById(id)).thenReturn(Optional.of(calendarEntity));
        when(calendarMapperJpa.toEntity(calendar)).thenCallRealMethod();
        when(calendarRepository.save(calendarEntity)).thenReturn(calendarEntity);
        when(calendarMapperJpa.toDomain(calendarEntity)).thenCallRealMethod();
        when(calendarDocumentMapper.toDocument(calendar)).thenCallRealMethod();

        // when
        Calendar updatedCalendar = calendarPersistenceAdapter.updateCalendar(calendar);
        TransactionSynchronizationManager.getSynchronizations().stream().findFirst().get().afterCommit();

        // then
        verify(calendarRepository, times(1)).findById(id);
        verify(calendarRepository, times(1)).save(calendarEntity);
        verify(calendarDocumentRepository, times(1)).deleteById(id);
        verify(calendarDocumentRepository, times(1)).save(calendarDocument);
        assertEquals(calendar.getTitle(), updatedCalendar.getTitle());
        assertEquals(calendarDocument.getTitle(), updatedCalendar.getTitle());
    }

    @Test
    void shouldFindAllCalendars() {
        // given
        List<CalendarEntity> calendarEntities = Arrays.asList(
                CalendarEntity.builder().title("Calendar 1").build(),
                CalendarEntity.builder().title("Calendar 2").build()
        );

        when(calendarRepository.findAll()).thenReturn(calendarEntities);
        when(calendarMapperJpa.toDomain(any())).thenCallRealMethod();

        // when
        List<Calendar> calendars = calendarPersistenceAdapter.findAllCalendars();

        // then
        verify(calendarRepository).findAll();
        assertEquals(2, calendars.size());
        assertEquals("Calendar 1", calendars.get(0).getTitle());
        assertEquals("Calendar 2", calendars.get(1).getTitle());
    }

    @Test
    void shouldFindOneCalendar() {
        // given
        Long calendarId = 1L;
        CalendarEntity calendarEntity = CalendarEntity.builder().id(calendarId).title("Test Calendar").build();
        Calendar calendar = Calendar.builder().id(calendarId).title("Test Calendar").build();


        when(calendarRepository.findById(calendarId)).thenReturn(Optional.of(calendarEntity));
        when(calendarMapperJpa.toDomain(calendarEntity)).thenCallRealMethod();

        // when
        Optional<Calendar> result = calendarPersistenceAdapter.findCalendarById(calendarId);

        // then
        verify(calendarRepository).findById(calendarId);
        assertTrue(result.isPresent());
        assertEquals(calendarEntity.getTitle(), result.get().getTitle());
    }

    @Test
    void shouldReturnTrueWhenOwner() {
        // given
        UUID userId = UUID.randomUUID();
        Long resourceId = 1L;

        when(calendarRepository.existsByUserIdAndId(userId, resourceId)).thenReturn(true);

        // when
        boolean isOwner = calendarPersistenceAdapter.isOwner(userId, resourceId);

        // then
        verify(calendarRepository, times(1)).existsByUserIdAndId(userId, resourceId);
        assertTrue(isOwner);
    }

    @Test
    void shouldReturnFalseWhenNotOwner() {
        // given
        UUID userId = UUID.randomUUID();
        Long resourceId = 1L;

        when(calendarRepository.existsByUserIdAndId(userId, resourceId)).thenReturn(false);

        // when
        boolean isOwner = calendarPersistenceAdapter.isOwner(userId, resourceId);

        // then
        verify(calendarRepository).existsByUserIdAndId(userId, resourceId);
        assertFalse(isOwner);
    }

    @Test
    void shouldReindexAllCalendars() {
        // given
        List<CalendarEntity> calendarEntities = Arrays.asList(
                CalendarEntity.builder().id(1L).title("Calendar 1").build(),
                CalendarEntity.builder().id(2L).title("Calendar 2").build()
        );
        List<CalendarDocument> calendarDocuments = Arrays.asList(
                CalendarDocument.builder().id(1L).title("Calendar 1").build(),
                CalendarDocument.builder().id(2L).title("Calendar 2").build()
        );

        when(calendarRepository.findAll()).thenReturn(calendarEntities);
        when(calendarMapperJpa.toDomain(any())).thenCallRealMethod();
        when(calendarDocumentMapper.toDocument(any())).thenCallRealMethod();

        // when
        calendarPersistenceAdapter.reindexAll();
        TransactionSynchronizationManager.getSynchronizations().stream().findFirst().get().afterCommit();

        // then
        verify(calendarDocumentRepository, times(1)).deleteAll();
        verify(calendarDocumentRepository, times(1)).saveAll(calendarDocuments);
    }


    @Test
    void shouldReindexOneCalendar() {
        // given
        Long calendarId = 1L;
        Calendar calendar = Calendar.builder().id(calendarId).title("Test Calendar").build();
        CalendarDocument calendarDocument = CalendarDocument.builder().id(calendarId).title("Test Calendar").build();

        when(calendarDocumentMapper.toDocument(calendar)).thenCallRealMethod();

        // when
        calendarPersistenceAdapter.reindexOne(calendar);
        TransactionSynchronizationManager.getSynchronizations().stream().findFirst().get().afterCommit();

        // then
        verify(calendarDocumentRepository, times(1)).deleteById(calendarId);
        verify(calendarDocumentRepository, times(1)).save(calendarDocument);
    }

    @Test
    void shouldReturnCalendarsFromMultiFieldFuzzySearch() {
        // given
        String input = "test";
        UUID userId = UUID.randomUUID();
        NativeQuery esQuery = NativeQuery.builder().build();

        List<SearchHit<CalendarDocument>> hits = Arrays.asList(
                new SearchHit<>(null, null, null, 0L, null, null, null, null, null, null, CalendarDocument.builder().id(1L).title("Calendar 1").build()),
                new SearchHit<>(null, null, null, 0L, null, null, null, null, null, null, CalendarDocument.builder().id(2L).title("Calendar 2").build())
        );

        SearchHits<CalendarDocument> searchHitList = new SearchHitsImpl<CalendarDocument>(2L, TotalHitsRelation.OFF, 0L, null, null, hits, null, null);

        when(calendarDocumentRepository.multiFieldFuzzySearchQuery(any(), any())).thenReturn(esQuery);
        when(elasticsearchOperations.search(any(Query.class), eq(CalendarDocument.class))).thenReturn(searchHitList);
        when(calendarDocumentMapper.toDomain(any())).thenCallRealMethod();

        // when
        List<Calendar> calendars = calendarPersistenceAdapter.multiFieldFuzzySearch(input, Optional.of(userId));

        // then
        verify(calendarDocumentRepository, times(1)).multiFieldFuzzySearchQuery(input, Optional.of(userId));
        assertEquals(2, calendars.size());
    }

    @Test
    void shouldReturnCalendarsFromTop5SuggestionSearch() {
        // given
        String input = "test";
        UUID userId = UUID.randomUUID();
        NativeQuery esQuery = NativeQuery.builder().build();

        List<SearchHit<CalendarDocument>> hits = Arrays.asList(
                new SearchHit<>(null, null, null, 0L, null, null, null, null, null, null, CalendarDocument.builder().id(1L).title("Calendar 1").build()),
                new SearchHit<>(null, null, null, 0L, null, null, null, null, null, null, CalendarDocument.builder().id(2L).title("Calendar 2").build())
        );

        SearchHits<CalendarDocument> searchHitList = new SearchHitsImpl<>(2L, TotalHitsRelation.OFF, 0L, null, null, hits, null, null);

        when(calendarDocumentRepository.top5SuggestionSearchQuery(any(), any())).thenReturn(esQuery);
        when(elasticsearchOperations.search(any(Query.class), eq(CalendarDocument.class))).thenReturn(searchHitList);
        when(calendarDocumentMapper.toDomain(any())).thenCallRealMethod();

        // when
        List<Calendar> calendars = calendarPersistenceAdapter.top5SuggestionSearch(input, Optional.of(userId));

        // then
        verify(calendarDocumentRepository, times(1)).top5SuggestionSearchQuery(input, Optional.of(userId));
        assertEquals(2, calendars.size());
    }

    @Test
    void shouldThrowFromMultiFieldFuzzySearchWithNoInput() {
        assertThrows(IllegalArgumentException.class, () -> calendarPersistenceAdapter.multiFieldFuzzySearch(null, Optional.of(UUID.randomUUID())));
    }

    @Test
    void shouldThrowFromTop5SuggestionSearchWithNoInput() {
        assertThrows(IllegalArgumentException.class, () -> calendarPersistenceAdapter.top5SuggestionSearch(null, Optional.of(UUID.randomUUID())));
    }
}
