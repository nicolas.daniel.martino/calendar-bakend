# Hexagonal architecture spring boot calendar application

## Main objective:

Have a deployed application that leverages postgres an elasticsearch to provide efficient querying for calendar events.

## Calendar API spec

https://www.w3.org/TR/2011/WD-calendar-api-20110419/

# Architecture:

    - Multi module maven project
    - open idconnect (keycloak)
    - postgres
    - elasticsearch

# Swagger

address: http://localhost:8085/swagger-ui.html

docs: http://localhost:8085/v3/api-docs/

# Docker

## local env

docker-compose -f containers/docker-compose.yml -f containers/docker-compose-local.yml -p "calendar_local" up

with test context:

docker-compose -f containers/docker-compose.yml -f containers/docker-compose-test.yml -p "calendar_test" up

## Package and Run app

mvn clean install java -jar .\launcher\target\launcher-1.0-SNAPSHOT.jar --spring.profiles.active=local

### Mvn integration tests
https://fndomoraes.medium.com/running-docker-containers-on-maven-integration-tests-ee071bcd743f


docker-compose -f containers/docker-compose.yml -f containers/docker-compose-integration-test.yml -p "calendar_test" up --no-deps --build --exit-code-from service-to-test

## Reset all

docker stop $(docker ps -a -q)

docker rm $(docker ps -a -q)

docker rmi $(docker images -a -q)

docker volume prune

Restart init db script by destroying volumes e.g. in ./infra/data

# Authorisation server

https://www.keycloak.org/getting-started/getting-started-docker

https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow

https://auth0.com/docs/get-started/authentication-and-authorization-flow/authorization-code-flow

https://medium.com/@bcarunmail/securing-rest-api-using-keycloak-and-spring-oauth2-6ddf3a1efcc2

# Done:

Add simple ci/cd run mvn tests on push

Add integration tests

Add maptruct

Add validator no id on create change name

Add postgres and migration manager

add swagger

Add keycloak to prj

Add swagger open id connect

Protect object access (check object owner)

Add aspect tests

migrate keycloak container to last version

add security tests (mvc)

Add elasticsearch to prj

# TODOS:

TODO add integration to gitlab ci

TODO change from role based to permission only based authorisation using client based ressource acces in token see https://usmanshahid.medium.com/levels-of-access-control-through-keycloak-part-3-access-control-through-roles-and-tokens-a1744c04895e

TODO replace transaction synchronisation manager with annotation: https://medium.com/javarevisited/scheduling-methods-to-execute-post-transaction-commit-using-spring-transactionsynchronization-8df3fa4f4e37

TODO fix swagger cors (add api gateway?)

TODO migrate to spring 3

TODO Fix update one calendar :
curl --location --request POST 'http://localhost:8085/api/v1/calendars' \
--header 'Content-Type: application/json' \
--data-raw '{
"id":"1234",
"title": "first one",
"description": "the first claendar"

}'

TODO Add login to keycloak in prj

TODO Add generic security on objects access

TODO Add generic way to add action to a companion action tablbe to a domain object

TODO ADD ELK

TODO SOA kafka commit log

TODO when admin asks for calendar it should get user informations in the response

TODO verify disable scsrf in config is safe


Comment from thierry:
- domaine manque realtion user owns calendar
- modiule domaine c'est j'utilise que du java de base et ça doit pouvoir représenter les obejts de mon domaine et les intéractions  qu'ils ont entre eux

- pcpes solids: 
- ressource not found exception not in domain
- use record instead @Data
- write function with user types as parameters e.g. admin / user etc

