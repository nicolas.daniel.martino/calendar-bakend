package org.example.calendar.services;

import org.example.calendar.data.Calendar;
import org.example.calendar.mapper.CalendarMapper;
import org.example.calendar.mapper.CalendarMapperImpl;
import org.example.calendar.spi.CalendarPersistencePort;
import org.example.user.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CalendarServiceTest {

    @InjectMocks
    CalendarServiceImpl calendarServiceImpl;
    @Mock
    CalendarPersistencePort calendarPersistencePort;

    CalendarMapper calendarMapper = Mockito.mock(CalendarMapperImpl.class);

    @Test
    void should_add_new_calendar() {
        //Given
        Calendar dto = getTestCalendar();

        when(calendarPersistencePort.saveCalendar(any())).thenReturn(dto);
        //When
        Calendar res = calendarServiceImpl.addCalendar(User.builder().id(UUID.randomUUID()).build(), dto);
        //Then
        verify(calendarPersistencePort, times(1)).saveCalendar(any());
        Assertions.assertEquals(dto, res);
    }

    @Test
    void should_update_calendar() {
        //Given
        Calendar dto = getTestCalendar();

        Calendar calendarToUpdate = Calendar.builder()
                .id(1L)
                .title("test to update")
                .description("testy to update")
                .userId(UUID.randomUUID())
                .build();

        when(calendarPersistencePort.findCalendarByIdAndUserId(any(), any())).thenReturn(Optional.ofNullable(calendarToUpdate));
        when(calendarPersistencePort.updateCalendar(any())).thenReturn(dto);
        doCallRealMethod().when(calendarMapper).merge(any(), any());

        //When
        Calendar res = calendarServiceImpl.updateCalendar(User.builder().id(UUID.randomUUID()).build(), dto.getId(), dto);
        //Then
        verify(calendarPersistencePort, times(1)).updateCalendar(any());
        Assertions.assertEquals(dto, res);
    }

    @Test
    void should_delete_calendar() {
        //Given
        Calendar calendarToDelete = getTestCalendar();

        when(calendarPersistencePort.findCalendarByIdAndUserId(any(), any())).thenReturn(Optional.ofNullable(calendarToDelete));
        //When
        calendarServiceImpl.deleteCalendarById(User.builder().id(UUID.randomUUID()).build(), calendarToDelete.getId());
        //Then
        verify(calendarPersistencePort, times(1)).deleteCalendarById(any());
    }

    @Test
    void should_get_calendars() {
        //Given
        Calendar calendarToGet = getTestCalendar();

        when(calendarPersistencePort.findAllCalendarsByUserId(any())).thenReturn(List.of(calendarToGet));
        //When
        List<Calendar> result = calendarServiceImpl.getCalendars(User.builder().id(UUID.randomUUID()).build());
        //Then
        verify(calendarPersistencePort, times(1)).findAllCalendarsByUserId(any());
        assertTrue(result.contains(calendarToGet));
    }

    @Test
    void should_get_calendars_by_id() {
        //Given
        Calendar calendarToGet = getTestCalendar();

        when(calendarPersistencePort.findCalendarByIdAndUserId(any(), any())).thenReturn(Optional.ofNullable(calendarToGet));
        //When
        Optional<Calendar> result = calendarServiceImpl.getCalendarById(User.builder().id(UUID.randomUUID()).build(), calendarToGet.getId());
        //Then
        verify(calendarPersistencePort, times(1)).findCalendarByIdAndUserId(any(), any());

        assertTrue(result.isPresent());
        assertEquals(calendarToGet, result.get());
    }

    @Test
    void should_have_owners() {
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.addCalendar(null, getTestCalendar()));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.updateCalendar(null, getTestCalendar().getId(), getTestCalendar()));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.deleteCalendarById(null, getTestCalendar().getId()));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.getCalendars(null));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.getCalendarById(null, getTestCalendar().getId()));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.multiFieldFuzzySearch(null, "test search"));
        assertThrows(IllegalArgumentException.class, () -> calendarServiceImpl.top5SuggestionSearch(null, "test search"));
    }

    private static Calendar getTestCalendar() {
        return Calendar.builder()
                .id(1L)
                .title("test")
                .description("testy")
                .userId(UUID.randomUUID())
                .build();
    }
}
