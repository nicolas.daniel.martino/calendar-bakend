package org.example.config.security;

import org.example.config.exceptions.ResourceOwnershipException;
import org.example.config.security.ownership.CheckOwnership;
import org.example.config.security.ownership.OwnershipCheckable;
import org.example.config.security.ownership.OwnershipCheckerAspect;
import org.example.config.security.ownership.ResourceName;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.aop.framework.AopProxy;
import org.springframework.aop.framework.DefaultAopProxyFactory;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OwnershipAspectTest {
    @Test
    public void whenInvokingWithProperOwnership() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentIsAdmin()).thenReturn(false);
        when(userServiceApi.getCurrent()).thenReturn(User.builder().id(UUID.randomUUID()).build());

        OwnershipCheckable ownershipCheckable = mock(OwnershipCheckable.class);
        when(ownershipCheckable.getResourceName()).thenReturn(ResourceName.CALENDAR);
        when(ownershipCheckable.isOwner(any(), any())).thenReturn(true);

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new OwnershipCheckerAspect(userServiceApi, List.of(ownershipCheckable)));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //when
        boolean result = proxiesTestClass.someMethod(0L);

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void adminAlwaysHasAccess() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentIsAdmin()).thenReturn(true);

        OwnershipCheckable ownershipCheckable = mock(OwnershipCheckable.class);
        when(ownershipCheckable.getResourceName()).thenReturn(ResourceName.CALENDAR);

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new OwnershipCheckerAspect(userServiceApi, List.of(ownershipCheckable)));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //when
        boolean result = proxiesTestClass.someMethod(0L);

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void throwsWhenInvokingWithUnauthorizedPermission() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentIsAdmin()).thenReturn(false);
        when(userServiceApi.getCurrent()).thenReturn(User.builder().id(UUID.randomUUID()).build());

        OwnershipCheckable ownershipCheckable = mock(OwnershipCheckable.class);
        when(ownershipCheckable.getResourceName()).thenReturn(ResourceName.CALENDAR);
        when(ownershipCheckable.isOwner(any(), any())).thenReturn(false);

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new OwnershipCheckerAspect(userServiceApi, List.of(ownershipCheckable)));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //When
        Assertions.assertThrows(ResourceOwnershipException.class, () -> proxiesTestClass.someMethod(0L));
    }

    class TestClass {
        @CheckOwnership(type = ResourceName.CALENDAR, id = "testId")
        boolean someMethod(Long testId) {
            return true;
        }
    }
}
