package org.example.config.security;

import org.example.config.exceptions.PrivilegeException;
import org.example.config.security.role.CheckRole;
import org.example.config.security.role.RoleCheckerAspect;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.aop.framework.AopProxy;
import org.springframework.aop.framework.DefaultAopProxyFactory;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleAspectTest {

    @Test
    public void whenInvokingWithPromotorRole() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentUserHasRole(any())).thenReturn(true);

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new RoleCheckerAspect(userServiceApi));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //when
        Assertions.assertTrue(proxiesTestClass::someMethod);
    }

    @Test
    public void throwsWhenInvokingWithUnauthorizedRole() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentUserHasRole(any())).thenReturn(false);
        when(userServiceApi.getCurrent()).thenReturn(User.builder().id(UUID.randomUUID()).build());

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new RoleCheckerAspect(userServiceApi));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //when
        Assertions.assertThrows(PrivilegeException.class, proxiesTestClass::someMethod);
    }

    @Test
    public void adminHasAlwaysAccess() {
        //given
        UserServiceApi userServiceApi = mock(UserServiceApi.class);
        when(userServiceApi.currentIsAdmin()).thenReturn(true);

        AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(new TestClass());
        aspectJProxyFactory.addAspect(new RoleCheckerAspect(userServiceApi));

        DefaultAopProxyFactory proxyFactory = new DefaultAopProxyFactory();
        AopProxy aopProxy = proxyFactory.createAopProxy(aspectJProxyFactory);

        final TestClass proxiesTestClass = (TestClass) aopProxy.getProxy();

        //when
        Assertions.assertTrue(proxiesTestClass::someMethod);
    }

    class TestClass {
        @CheckRole(RoleEnum.PROMOTOR)
        boolean someMethod() {
            return true;
        }
    }
}
