package org.example.user.services;

import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.AuthenticationApi;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    AuthenticationApi authenticationApi;

    @Test
    void whenGetCurrentUserWithAuthenticatedUser() {
        //given
        UserServiceApi userServiceApi = new UserServiceImpl(authenticationApi);
        User expectedUser = User.builder().id(UUID.randomUUID()).build();
        when(authenticationApi.getAuthenticatedUser()).thenReturn(Optional.ofNullable(expectedUser));

        //when
        User actual = userServiceApi.getCurrent();

        //then
        Assertions.assertEquals(actual, expectedUser);
    }

    @Test
    void whenGetCurrentUserWithNOAuthenticatedUserTheThrow() {
        //given
        UserServiceApi userServiceApi = new UserServiceImpl(authenticationApi);
        when(authenticationApi.getAuthenticatedUser()).thenReturn(Optional.empty());

        //when
        Assertions.assertThrows(AuthenticationCredentialsNotFoundException.class, userServiceApi::getCurrent);
    }

    @Test
    void whenGetIsAdminWithAdminRole() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().roles(Set.of(RoleEnum.PRODUCER, RoleEnum.ADMIN)).build());
        when(userServiceApi.currentIsAdmin()).thenCallRealMethod();

        //then
        Assertions.assertTrue(userServiceApi.currentIsAdmin());
    }

    @Test
    void whenGetIsAdminNoAdminRole() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().roles(Set.of(RoleEnum.PRODUCER)).build());
        when(userServiceApi.currentIsAdmin()).thenCallRealMethod();

        //then
        Assertions.assertFalse(userServiceApi.currentIsAdmin());
    }

    @Test
    void whenCurrentUserHasRole() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().roles(Set.of(RoleEnum.PRODUCER)).build());
        when(userServiceApi.currentUserHasRole(any())).thenCallRealMethod();

        //then
        Assertions.assertTrue(userServiceApi.currentUserHasRole(RoleEnum.PRODUCER));
    }

    @Test
    void whenCurrentUserHasNoRole() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().roles(Set.of(RoleEnum.CONSUMER)).build());
        when(userServiceApi.currentUserHasRole(any())).thenCallRealMethod();

        //then
        Assertions.assertFalse(userServiceApi.currentUserHasRole(RoleEnum.PRODUCER));
    }

    @Test
    void whenCurrentUserHasPermission() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().permissions(Set.of(PermissionEnum.VIEW_PROFILE)).build());
        when(userServiceApi.currentUserHasPermission(any())).thenCallRealMethod();

        //then
        Assertions.assertTrue(userServiceApi.currentUserHasPermission(PermissionEnum.VIEW_PROFILE));
    }

    @Test
    void whenCurrentUserHasNoPermission() {
        //given
        UserServiceApi userServiceApi = Mockito.mock(UserServiceImpl.class);

        //when
        when(userServiceApi.getCurrent()).thenReturn(User.builder().permissions(Set.of(PermissionEnum.VIEW_PROFILE)).build());
        when(userServiceApi.currentUserHasPermission(any())).thenCallRealMethod();

        //then
        Assertions.assertFalse(userServiceApi.currentUserHasPermission(PermissionEnum.MANAGE_ACCOUNT));
    }

}
