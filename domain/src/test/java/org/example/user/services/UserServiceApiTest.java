package org.example.user.services;

import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.UserServiceApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class UserServiceApiTest {

    @Test
    void check_permissions(){
        //given
        Set<PermissionEnum> permissionsToCheck = Set.of(PermissionEnum.VIEW_PROFILE, PermissionEnum.MANAGE_ACCOUNT_LINKS);

        Assertions.assertTrue(UserServiceApi.checkPermissions(permissionsToCheck, PermissionEnum.VIEW_PROFILE)); // has one permission
        Assertions.assertTrue(UserServiceApi.checkPermissions(permissionsToCheck, PermissionEnum.VIEW_PROFILE, PermissionEnum.MANAGE_ACCOUNT_LINKS)); // has all permission
        Assertions.assertFalse(UserServiceApi.checkPermissions(permissionsToCheck, PermissionEnum.VIEW_PROFILE, PermissionEnum.MANAGE_ACCOUNT)); // lacks one permission
    }

    @Test
    void check_roles(){
        //given
        Set<RoleEnum> permissionsToCheck = Set.of(RoleEnum.PRODUCER, RoleEnum.ADMIN);

        Assertions.assertTrue(UserServiceApi.checkRoles(permissionsToCheck, RoleEnum.ADMIN)); // has one role
        Assertions.assertTrue(UserServiceApi.checkRoles(permissionsToCheck, RoleEnum.ADMIN, RoleEnum.PRODUCER)); // has all roles
        Assertions.assertFalse(UserServiceApi.checkRoles(permissionsToCheck, RoleEnum.ADMIN, RoleEnum.CONSUMER)); // lacks one role
    }
}
