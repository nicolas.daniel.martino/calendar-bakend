package org.example.config.search;

public interface Searchable<T> {

    void reindexAll();
    void reindexOne(T element);
    void indexOne(T element);
}
