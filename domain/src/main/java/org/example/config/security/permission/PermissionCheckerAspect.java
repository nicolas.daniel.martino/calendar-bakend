package org.example.config.security.permission;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.example.config.exceptions.PrivilegeException;
import org.example.config.i18n.DomainError;
import org.example.config.security.role.RoleCheckerAspect;
import org.example.user.api.UserServiceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
public class PermissionCheckerAspect {

    private final UserServiceApi userServiceApi;
    private static final Logger LOG = LoggerFactory.getLogger(RoleCheckerAspect.class);

    public PermissionCheckerAspect(UserServiceApi userServiceApi) {
        this.userServiceApi = userServiceApi;
    }

    @Around("@annotation(org.example.config.security.permission.CheckPermission)")
    public Object intercept(final ProceedingJoinPoint pjp) throws Throwable {
        if (userServiceApi.currentIsAdmin()) {
            return pjp.proceed(pjp.getArgs());
        }

        final MethodSignature signature = (MethodSignature) pjp.getSignature();
        final CheckPermission annotation = signature.getMethod().getAnnotation(CheckPermission.class);
        if (userServiceApi.currentUserHasPermission(annotation.value())) {
            return pjp.proceed(pjp.getArgs());
        } else {
            LOG.error("User has no permission matching {}", annotation.value());
            throw new PrivilegeException(DomainError.NO_PERMISSION, annotation.value().toString(), userServiceApi.getCurrent().getId());
        }
    }
}
