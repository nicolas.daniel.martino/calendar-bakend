package org.example.config.security.ownership;

import java.util.UUID;

public interface OwnershipCheckable {

    boolean isOwner(UUID userId, Long resourceId);

    ResourceName getResourceName();
}
