package org.example.config.security.role;

import org.springframework.security.core.GrantedAuthority;

public enum RoleEnum implements GrantedAuthority {
    PROMOTOR,
    PRODUCER,
    CONSUMER,
    ADMIN;

    public static RoleEnum fromKebabCase(String role) {
        String permissionAsScreamingSnakeCase = role.replaceAll("-", "_").toUpperCase();
        return RoleEnum.valueOf(permissionAsScreamingSnakeCase);
    }

    @Override
    public String getAuthority() {
        return this.name();
    }
}
