package org.example.config.security.role;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.example.config.exceptions.PrivilegeException;
import org.example.config.i18n.DomainError;
import org.example.user.api.UserServiceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RoleCheckerAspect {
    private final UserServiceApi userServiceApi;
    private static final Logger LOG = LoggerFactory.getLogger(RoleCheckerAspect.class);

    public RoleCheckerAspect(UserServiceApi userServiceApi) {
        this.userServiceApi = userServiceApi;
    }

    @Around("@annotation(org.example.config.security.role.CheckRole)")
    public Object intercept(final ProceedingJoinPoint pjp) throws Throwable {
        if (userServiceApi.currentIsAdmin()) {
            return pjp.proceed(pjp.getArgs());
        }
        
        final MethodSignature signature = (MethodSignature) pjp.getSignature();
        final CheckRole annotation = signature.getMethod().getAnnotation(CheckRole.class);
        if (userServiceApi.currentUserHasRole(annotation.value())) {
            return pjp.proceed(pjp.getArgs());
        } else {
            LOG.error("User does not have role {}", annotation.value());
            throw new PrivilegeException(DomainError.NO_ROLE, annotation.value().toString(), userServiceApi.getCurrent().getId());
        }
    }
}
