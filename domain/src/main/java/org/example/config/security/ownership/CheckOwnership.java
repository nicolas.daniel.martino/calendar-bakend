package org.example.config.security.ownership;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Check current authenticated user has ownership of a resource
 * <p>
 * Admin owns all
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckOwnership {
    ResourceName type();

    String id();
}
