package org.example.config.security.permission;

import org.springframework.security.core.GrantedAuthority;

public enum PermissionEnum implements GrantedAuthority {
    MANAGE_ACCOUNT,
    MANAGE_ACCOUNT_LINKS,
    VIEW_PROFILE;

    public static PermissionEnum fromKebabCase(String permission) {
        String permissionAsScreamingSnakeCase = permission.replaceAll("-", "_").toUpperCase();
        return PermissionEnum.valueOf(permissionAsScreamingSnakeCase);
    }

    @Override
    public String getAuthority() {
        return this.name();
    }
}
