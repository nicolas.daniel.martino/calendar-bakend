package org.example.config.security.ownership;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.example.config.exceptions.ResourceOwnershipException;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.function.Function.identity;

@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 2)
public class OwnershipCheckerAspect {

    private final UserServiceApi userServiceApi;
    private final Map<ResourceName, OwnershipCheckable> ownershipCheckableMap = new EnumMap<>(ResourceName.class);

    public OwnershipCheckerAspect(UserServiceApi userServiceApi,
                                  List<OwnershipCheckable> ownershipCheckableList) {
        this.userServiceApi = userServiceApi;
        for (OwnershipCheckable ownershipCheckable: ownershipCheckableList) {
            ownershipCheckableMap.put(ownershipCheckable.getResourceName(), ownershipCheckable);
        }
    }

    @Around("@annotation(org.example.config.security.ownership.CheckOwnership)")
    public Object intercept(final ProceedingJoinPoint pjp) throws Throwable {
        User currentUser = userServiceApi.getCurrent();

        // Do not check ownership for admin
        if (userServiceApi.currentIsAdmin()) {
            return pjp.proceed(pjp.getArgs());
        }

        final MethodSignature signature = (MethodSignature) pjp.getSignature();
        final CheckOwnership annotation = signature.getMethod().getAnnotation(CheckOwnership.class);
        final String idParamName = annotation.id();
        final ResourceName type = annotation.type();

        Long resourceId = null;
        for (int i = 0; i < signature.getParameterNames().length; i++) {
            String name = signature.getParameterNames()[i];
            if (name.equals(idParamName)) {
                resourceId = (Long) pjp.getArgs()[i];
            }
        }
        if (isNull(resourceId)) {
            throw new IllegalArgumentException("Could find resource id");
        }

        if (!this.ownershipCheckableMap.get(type).isOwner(currentUser.getId(), resourceId)) {
            throw new ResourceOwnershipException(resourceId, currentUser.getId());
        }

        return pjp.proceed(pjp.getArgs());
    }

}
