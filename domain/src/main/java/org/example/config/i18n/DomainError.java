package org.example.config.i18n;

public enum DomainError {
    NO_PERMISSION,
    NO_ROLE
}
