package org.example.config.exceptions;

import org.example.config.i18n.DomainError;

import java.util.UUID;

import static java.lang.String.format;

/**
 * Exception thrown when user does not have the correct permissions to access a resource
 */
public class PrivilegeException extends RuntimeException {


    public PrivilegeException(DomainError domainError, UUID userId) {
        super(format("%s for user %s", domainError.name(), userId));
    }

    public PrivilegeException(DomainError domainError, String resourceName, UUID userId) {
        super(format("%s '%s' for user %s", domainError.name(), resourceName, userId));
    }
}
