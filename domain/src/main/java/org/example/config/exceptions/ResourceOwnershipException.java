package org.example.config.exceptions;

import java.util.UUID;

public class ResourceOwnershipException extends RuntimeException {
    public ResourceOwnershipException(long resourceId, UUID userId) {
        super(String.format("Resource with id %d could not be found with user id %s", resourceId, userId));
    }
}
