package org.example.user.api;

import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.data.Admin;
import org.example.user.data.User;

import java.util.Set;

import static java.util.Objects.isNull;

public interface UserServiceApi {
    boolean currentUserHasPermission(PermissionEnum... permissions);

    boolean currentUserHasRole(RoleEnum... roles);

    boolean currentIsAdmin();

    User getCurrent();

    static boolean checkRoles(Set<RoleEnum> rolesToCheck, RoleEnum... roles) {
        for (RoleEnum role : roles) {
            if (!rolesToCheck.contains(role)) {
                return false;
            }
        }
        return true;
    }

    static boolean checkPermissions(Set<PermissionEnum> permissionsToCheck, PermissionEnum... permissions) {
        for (PermissionEnum permission : permissions) {
            if (!permissionsToCheck.contains(permission)) {
                return false;
            }
        }
        return true;
    }

    static void hasOwner(User calendarOwner) {
        if (isNull(calendarOwner)) {
            throw new IllegalArgumentException("User is needed");
        }
    }

    static void hasAdmin(Admin admin) {
        if (isNull(admin)) {
            throw new IllegalArgumentException("Admin is needed");
        }
    }
}
