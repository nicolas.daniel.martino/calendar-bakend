package org.example.user.api;

import org.example.user.data.User;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface AuthenticationApi {
    Authentication get();

    Optional<User> getAuthenticatedUser();

}
