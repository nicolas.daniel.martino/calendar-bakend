package org.example.user.services;

import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.example.user.api.AuthenticationApi;
import org.example.user.api.UserServiceApi;
import org.example.user.data.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
class UserServiceImpl implements UserServiceApi {
    private static final Logger LOG = LoggerFactory.getLogger(UserServiceApi.class);

    private final AuthenticationApi authenticationApi;

    UserServiceImpl(AuthenticationApi authenticationApi) {
        this.authenticationApi = authenticationApi;
    }

    @Override
    public boolean currentUserHasPermission(PermissionEnum... permissions) {
        Set<PermissionEnum> currentPermissions = getCurrent().getPermissions();
        return UserServiceApi.checkPermissions(currentPermissions, permissions);
    }

    @Override
    public boolean currentUserHasRole(RoleEnum... roles) {
        Set<RoleEnum> currentRoles = getCurrent().getRoles();
        return UserServiceApi.checkRoles(currentRoles, roles);
    }

    @Override
    public boolean currentIsAdmin() {
        return getCurrent().getRoles().stream().anyMatch(roleEnum -> roleEnum.equals(RoleEnum.ADMIN));
    }
    
    /**
     * Builds the current user from auth context
     * Register user if it does not exist and updates connection dates
     *
     * @return User
     */
    @Override
    public User getCurrent() {
        Optional<User> authenticatedUser = authenticationApi.getAuthenticatedUser();
        if (authenticatedUser.isPresent()) {
            LOG.info("Found authenticated user with uuid {}", authenticatedUser.get().getId());
            return authenticatedUser.get();
        }

        throw new AuthenticationCredentialsNotFoundException("Current user could not be obtained from authentication object");
    }
}
