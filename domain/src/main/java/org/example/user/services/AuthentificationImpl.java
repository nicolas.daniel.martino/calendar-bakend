package org.example.user.services;

import org.example.user.api.AuthenticationApi;
import org.example.user.data.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class AuthentificationImpl implements AuthenticationApi {
    @Override
    public Authentication get() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public Optional<User> getAuthenticatedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            return Optional.of((User) principal);
        }

        return Optional.empty();
    }
}
