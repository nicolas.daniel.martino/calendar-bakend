package org.example.user.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.example.config.security.permission.PermissionEnum;
import org.example.config.security.role.RoleEnum;
import org.springframework.security.core.GrantedAuthority;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class User implements Principal {
    private UUID id;
    private String email;
    private Boolean verifiedEmail;
    private Set<PermissionEnum> permissions;
    private Set<RoleEnum> roles;

    @JsonIgnore
    public Set<GrantedAuthority> getGrantedAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        if (nonNull(permissions)) {
            authorities.addAll(permissions.stream().map(GrantedAuthority.class::cast).collect(Collectors.toSet()));
        }

        if (nonNull(roles)) {
            authorities.addAll(roles.stream().map(GrantedAuthority.class::cast).collect(Collectors.toSet()));
        }

        return authorities;
    }

    @Override
    public String getName() {
        return email;
    }
}
