package org.example.user.mapper;

import org.example.user.data.Admin;
import org.example.user.data.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    Admin from(User user);
}
