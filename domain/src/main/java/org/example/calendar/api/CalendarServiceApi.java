package org.example.calendar.api;

import org.example.calendar.data.Calendar;
import org.example.config.search.Searchable;
import org.example.user.data.Admin;
import org.example.user.data.User;

import java.util.List;
import java.util.Optional;

public interface CalendarServiceApi extends Searchable<Calendar> {
    Calendar addCalendar(User user, Calendar Calendar);

    Calendar updateCalendar(User user, Long id, Calendar calendar);

    void deleteCalendarById(User calendarOwner, Long id);

    List<Calendar> getCalendars(User calendarOwner);

    List<Calendar> getCalendars(Admin admin);

    Optional<Calendar> getCalendarById(User calendarOwner, Long CalendarId);

    List<Calendar> multiFieldFuzzySearch(User calendarsOwner, String input);

    List<Calendar> multiFieldFuzzySearch(Admin admin, String input);

    List<Calendar> top5SuggestionSearch(User calendarsOwner, String input);

    void reindexAll();

    void reindexOne(Calendar element);

    void indexOne(Calendar calendar);


}
