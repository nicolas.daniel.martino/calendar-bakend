package org.example.calendar.spi;

import org.example.calendar.data.Calendar;
import org.example.config.search.Searchable;
import org.example.config.security.ownership.OwnershipCheckable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CalendarPersistencePort extends OwnershipCheckable, Searchable<Calendar> {
    Calendar saveCalendar(Calendar calendar);

    void deleteCalendarById(Long id);

    Calendar updateCalendar(Calendar calendar);

    List<Calendar> findAllCalendars();

    List<Calendar> findAllCalendarsByUserId(UUID userId);

    Optional<Calendar> findCalendarById(Long calendarId);

    Optional<Calendar> findCalendarByIdAndUserId(Long calendarId, UUID userId);

    List<Calendar> multiFieldFuzzySearch(String input, Optional<UUID> userId);


    List<Calendar> top5SuggestionSearch(String input, Optional<UUID> userId);
}
