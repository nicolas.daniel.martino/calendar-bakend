package org.example.calendar.services;

import org.example.calendar.api.CalendarServiceApi;
import org.example.calendar.data.Calendar;
import org.example.calendar.mapper.CalendarMapper;
import org.example.calendar.spi.CalendarPersistencePort;
import org.example.config.exceptions.ResourceNotFoundException;
import org.example.user.data.Admin;
import org.example.user.data.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.example.user.api.UserServiceApi.hasAdmin;
import static org.example.user.api.UserServiceApi.hasOwner;

@Service
public class CalendarServiceImpl implements CalendarServiceApi {

    private final CalendarPersistencePort calendarPersistencePort;
    private final CalendarMapper calendarMapper;

    public CalendarServiceImpl(CalendarPersistencePort calendarPersistencePort,
                               CalendarMapper calendarMapper) {
        this.calendarPersistencePort = calendarPersistencePort;
        this.calendarMapper = calendarMapper;
    }

    @Override
    public Calendar addCalendar(User calendarOwner, Calendar calendar) {
        hasOwner(calendarOwner);
        calendar.setUserId(calendarOwner.getId());
        return calendarPersistencePort.saveCalendar(calendar);
    }


    @Override
    public Calendar updateCalendar(User calendarOwner, Long id, Calendar calendar) {
        hasOwner(calendarOwner);
        Calendar calendarToUpdate = findCalendarByIdAndUserIdOrThrow(id, calendarOwner.getId());
        calendarMapper.merge(calendar, calendarToUpdate);
        return calendarPersistencePort.updateCalendar(calendarToUpdate);
    }

    @Override
    public void deleteCalendarById(User calendarOwner, Long id) {
        hasOwner(calendarOwner);
        findCalendarByIdAndUserIdOrThrow(id, calendarOwner.getId());
        calendarPersistencePort.deleteCalendarById(id);
    }

    private Calendar findCalendarByIdAndUserIdOrThrow(long calendarId, UUID userId) { // TODO return optionnal no exception
        //TODO change to exists by id
        Optional<Calendar> calendarToFetch = calendarPersistencePort.findCalendarByIdAndUserId(calendarId, userId);
        if (calendarToFetch.isEmpty()) {
            throw new ResourceNotFoundException(calendarId);
        }
        return calendarToFetch.get();
    }

    @Override
    public List<Calendar> getCalendars(User calendarOwner) {
        hasOwner(calendarOwner);
        return calendarPersistencePort.findAllCalendarsByUserId(calendarOwner.getId());
    }

    @Override
    public List<Calendar> getCalendars(Admin admin) {
        hasAdmin(admin);
        return calendarPersistencePort.findAllCalendars();
    }

    @Override
    public Optional<Calendar> getCalendarById(User calendarOwner, Long calendarId) {
        hasOwner(calendarOwner);
        return calendarPersistencePort.findCalendarByIdAndUserId(calendarId, calendarOwner.getId());
    }

    @Override
    public List<Calendar> multiFieldFuzzySearch(User calendarsOwner, String input) {
        hasOwner(calendarsOwner);
        return this.calendarPersistencePort.multiFieldFuzzySearch(input, Optional.of(calendarsOwner.getId()));
    }

    @Override
    public List<Calendar> multiFieldFuzzySearch(Admin admin, String input) {
        hasAdmin(admin);
        return this.calendarPersistencePort.multiFieldFuzzySearch(input, Optional.empty());
    }

    @Override
    public List<Calendar> top5SuggestionSearch(User calendarsOwner, String input) {
        hasOwner(calendarsOwner);
        return this.calendarPersistencePort.top5SuggestionSearch(input, Optional.of(calendarsOwner.getId()));
    }

    @Override
    public void reindexAll() {
        this.calendarPersistencePort.reindexAll();
    }

    @Override
    public void reindexOne(Calendar element) {
        this.calendarPersistencePort.reindexOne(element);
    }

    @Override
    public void indexOne(Calendar calendar) {
        this.calendarPersistencePort.indexOne(calendar);
    }
}
