package org.example.calendar.mapper;

import org.example.calendar.data.Calendar;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface CalendarMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "userId", ignore = true)
    void merge(Calendar incoming, @MappingTarget Calendar existing);
}
