package org.example.calendar.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Calendar {

    private Long id;

    private UUID userId;

    private String title;

    private String description;

}
